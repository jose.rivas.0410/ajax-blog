"use strict";

var blogRequest = $.ajax("data/ajax-blog-exercise.json");
    blogRequest.done(function (data) {
        console.log(data);

        var blogHTML = buildHTML(data)
        $("#my-posts").html(blogHTML)
    });

function buildHTML(blog) {

    var orderHTML = '';
    blog.forEach(function (myPost) {

        orderHTML += "<section>";
        orderHTML += "<dl>";
        orderHTML += "<dt><strong>" + "Web Blog #1:" + "</dt></strong>";
        orderHTML += "<dd>" + myPost.title + "</dd>";
        orderHTML += "<dd>" + myPost.content + "</dd>"
        orderHTML += "<dt><strong>" + "Date:" + "</dt></strong>";
        orderHTML += "<dd>" + myPost.date + "</dd>";
        orderHTML += "<dt><strong>" + "Categories:" + "</dt></strong>";
        orderHTML += "<dd>" + myPost.categories + "</dd>";
        orderHTML += "</dl>";
        orderHTML += "</section>"


    });
    return orderHTML
}
